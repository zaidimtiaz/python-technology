# Python technology

During this module you will learn how to setup and use PyCharm
to make the best use of it. You will learn as well how to create virtual 
environments that will make it possible to manage multiple projects with 
independent context and with independent libraries installed using `pip`.

Some of the topics that are going to be explained during this module are:
- environment configuration
- interpreter
- virtualenv (python3 `venV` module)
- pip
- PyCharm

## Presentation
Presentation is available under [this link](https://gitlab.com/sda-international/program/python/python-technology/wikis/uploads/e0da29832742609906dd85faad15dc80/Python_Technology.pdf).

# Further reading

## Links
1. https://pip.pypa.io/en/stable/
2. https://docs.python.org/3/library/venv.html
3. https://www.jetbrains.com/pycharm/documentation/